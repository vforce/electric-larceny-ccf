# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import multiprocessing
from pandas import DataFrame

def featurize_common(train_data) :
    return Featurize.common(train_data=train_data)

def featurize_consno(train_data) :
    return Featurize.consno(train_data=train_data)

class Featurize :
    @staticmethod
    def common(train_data) :
        """
        :param train_data: DataFrame
        :rtype: DataFrame
        """
        featurized = DataFrame()
        consNo = train_data['CONS_NO'] # type: DataFrame
        all_train = train_data.T[1:].T # type: DataFrame

        #最大减最小
        featurized['maxMinusMin'] = all_train.max(axis=1) - all_train.min(axis=1)

        # 均值
        featurized['mean'] = all_train.mean(axis=1)

        # 连续值的变化百分比
        featurized['continous_pct<5'] = 0
        featurized['continous_pct<10'] = 0
        featurized['continous_pct<15'] = 0
        featurized['continous_pct<20'] = 0
        featurized['continous_pct>20'] = 0
        featurized['continous_pct_pct>10'] = 0
        featurized['continous_pct_pct>20'] = 0
        featurized['continous_pct_pct>30'] = 0
        continousData = pd.read_csv('./input/data_continously.csv', index_col=0)
        continous_pct = continousData.pct_change().abs() # type: DataFrame
        continous_pct_pct = continous_pct.pct_change().abs() # type: DataFrame
        for userNo, row in all_train.iterrows():
            pct = continous_pct.loc[consNo[userNo], :] # type: DataFrame
            pct_pct = continous_pct_pct.loc[consNo[userNo], :] # type: DataFrame
            featurized.loc[userNo, 'continous_pct<5'] = pct[pct < 5].count()
            featurized.loc[userNo, 'continous_pct<10'] = pct[pct < 10].count()
            featurized.loc[userNo, 'continous_pct<15'] = pct[pct < 15].count()
            featurized.loc[userNo, 'continous_pct<20'] = pct[pct < 20].count()
            featurized.loc[userNo, 'continous_pct>20'] = pct[pct > 20].count()
            featurized.loc[userNo, 'continous_pct_pct>10'] = pct_pct[pct_pct > 10].count()
            featurized.loc[userNo, 'continous_pct_pct>20'] = pct_pct[pct_pct > 20].count()
            featurized.loc[userNo, 'continous_pct_pct>30'] = pct_pct[pct_pct > 30].count()

        # 标准差分布特征
        std = all_train.std(axis=1)
        featurized['std<20'] = 0
        featurized['std<40'] = 0
        featurized['std<60'] = 0
        featurized['std<80'] = 0
        featurized['std<100'] = 0
        featurized['std>100'] = 0
        for userNo, row in all_train.iterrows():
            if 0 < std[userNo] <= 20:
                featurized.at[userNo, 'std<20'] = 1
            elif 20 < std[userNo] <= 40:
                featurized.at[userNo, 'std<40'] = 1
            elif 40 < std[userNo] <= 60:
                featurized.at[userNo, 'std<60'] = 1
            elif 60 < std[userNo] <= 80:
                featurized.at[userNo, 'std<80'] = 1
            elif 80 < std[userNo] <= 100:
                featurized.at[userNo, 'std<100'] = 1
            else:
                featurized.at[userNo, 'std>100'] = 1

        # 缺失次数
        nonCount = all_train.T.isnull().sum()
        featurized['nonCount<40'] = 0
        featurized['nonCount<80'] = 0
        featurized['nonCount<100'] = 0
        featurized['nonCount<120'] = 0
        featurized['nonCount<150'] = 0
        featurized['nonCount<180'] = 0
        featurized['nonCount<200'] = 0
        featurized['nonCount>200'] = 0
        for userNo, row in all_train.iterrows():
            if 0 < nonCount[userNo] <= 40:
                featurized.at[userNo, 'nonCount<40'] = 1
            elif 40 < nonCount[userNo] <= 80:
                featurized.at[userNo, 'nonCount<80'] = 1
            elif 80 < nonCount[userNo] <= 100:
                featurized.at[userNo, 'nonCount<100'] = 1
            elif 100 < nonCount[userNo] <= 120:
                featurized.at[userNo, 'nonCount<120'] = 1
            elif 120 < nonCount[userNo] <= 150:
                featurized.at[userNo, 'nonCount<150'] = 1
            elif 150 < nonCount[userNo] <= 180:
                featurized.at[userNo, 'nonCount<180'] = 1
            elif 180 < nonCount[userNo] <= 200:
                featurized.at[userNo, 'nonCount<200'] = 1
            else:
                featurized.at[userNo, 'nonCount>200'] = 1

        # 较前日比低变化率的次数
        pct_change = all_train.pct_change(axis=1).abs()
        featurized['low_diff_count'] = pct_change[pct_change < 0.2].count(axis=1)

        # 用电量范围次数统计
        featurized['<0.2'] = all_train[all_train < 0.2].count(axis=1)
        featurized['<5'] = all_train[all_train < 5].count(axis=1)
        featurized['5'] = all_train[all_train > 5].count(axis=1)
        featurized['10'] = all_train[all_train > 10].count(axis=1)
        featurized['15'] = all_train[all_train > 15].count(axis=1)
        featurized['20'] = all_train[all_train > 20].count(axis=1)
        featurized['25'] = all_train[all_train > 25].count(axis=1)
        featurized['30'] = all_train[all_train > 30].count(axis=1)
        featurized['40'] = all_train[all_train > 40].count(axis=1)
        featurized['50'] = all_train[all_train > 50].count(axis=1)
        degree_range = pd.concat(
            [featurized['5'], featurized['10'], featurized['15'], featurized['20'], featurized['25'], featurized['30'],
             featurized['40'], featurized['50']
             ], axis=1) # type: DataFrame
        range_diff = degree_range.diff(axis=1).abs() # type: DataFrame
        range_diff.rename(columns={'10': '5-10', '15': '10-15', '20': '15-20', '25': '20-25', '30': '25-30',
                                   '40': '30-40', '50': '40-50'
                                   }, inplace=True)

        # 基于样本变化百分比的百分比
        pct_change = all_train.pct_change(axis=1).abs().replace('inf', 0)
        pct2_change = pct_change.pct_change(axis=1).abs().replace('inf', 0)
        featurized['pct2>50'] = pct2_change[pct2_change > 50].count(axis=1)

        # 样本变化百分比 + 均值的二维特征
        for userNo, row in all_train.iterrows():
            mean = row.mean()
            pct_change = row.pct_change().abs().replace('inf', 0)
            tmpFeature = DataFrame()
            tmpFeature.at[0, 'pct_std'] = pct_change.std()
            tmpFeature.at[0, 'pct=0'] = pct_change[pct_change == 0].count()
            tmpFeature.at[0, 'pct<0.5'] = pct_change[pct_change < 0.5].count() - tmpFeature.at[0, 'pct=0']
            tmpFeature.at[0, 'pct<1'] = pct_change[pct_change < 1].count() - tmpFeature.at[0, 'pct=0']
            tmpFeature.at[0, 'pct>1.5'] = pct_change[pct_change > 1.5].count()
            tmpFeature.at[0, 'pct>3'] = pct_change[pct_change > 3].count()
            tmpFeature.at[0, 'pct(0.5,1)]'] = tmpFeature.at[0, 'pct<1'] - tmpFeature.at[0, 'pct<0.5']
            tmpFeature.at[0, 'pct(1,1.5)'] = pct_change[pct_change < 1.5].count() - tmpFeature.at[0, 'pct<1'] - \
                                             tmpFeature.at[0, 'pct=0']
            tmpFeature.at[0, 'pct(1.5,3)'] = tmpFeature.at[0, 'pct>1.5'] - tmpFeature.at[0, 'pct>3']
            tmpFeature = tmpFeature.fillna(0)

            if mean < 1:
                addon_name = 'mean_<1_'
            elif mean < 5:
                addon_name = 'mean_<5_'
            elif mean < 10:
                addon_name = 'mean_<10_'
            elif mean < 15:
                addon_name = 'mean_<15_'
            else:
                addon_name = 'mean_>15_'

            for colName in tmpFeature.columns:
                if addon_name + colName not in featurized.columns:
                    featurized[addon_name + colName] = 0
                featurized.at[userNo, addon_name + colName] = tmpFeature.at[0, colName]

        # 用电量小于TOP20%的5%、5%-10%、10%-20%, 20%-30%的次数
        top20 = all_train.T.fillna(-1).quantile(q=0.2)
        featurized['top20:<5%'] = 0
        featurized['top20:5%-10%'] = 0
        featurized['top20:10%-20%'] = 0
        featurized['top20:20%-30%'] = 0
        for userNo, row in all_train.iterrows():
            featurized.loc[userNo, 'top20:<5%'] = row[row < top20[userNo] * 0.05].count()
            featurized.loc[userNo, 'top20:5%-10%'] = row[row <= top20[userNo] * 0.1].count() - featurized.loc[userNo, 'top20:<5%']
            featurized.loc[userNo, 'top20:10%-20%'] = row[row <= top20[userNo] * 0.2].count() - featurized.loc[userNo, 'top20:5%-10%'] - featurized.loc[userNo, 'top20:<5%']
            featurized.loc[userNo, 'top20:20%-30%'] = row[row <= top20[userNo] * 0.3].count() - featurized.loc[userNo, 'top20:20%-30%'] - featurized.loc[userNo, 'top20:5%-10%'] - featurized.loc[userNo, 'top20:<5%']

        # 平坡的最大长度、平坡次数
        tmpAllTrain = all_train[:]
        tmpAllTrain['maxSeriesLowDiffCount'] = 0
        tmpAllTrain['lowDiffSeiresCount'] = 0
        for userNo, row in all_train.iterrows():
            maxSeriesLowDiffCount = 0
            lowDiffSeiresCount = 0

            currentSeriesCount = 0
            lastValue = row[row.index[0]]
            for date in row.index[1:]:
                currentValue = row[date]
                if (currentValue - lastValue) / lastValue <= 0.2:
                    currentSeriesCount += 1
                else:
                    maxSeriesLowDiffCount = currentSeriesCount if currentSeriesCount > maxSeriesLowDiffCount else maxSeriesLowDiffCount
                    lowDiffSeiresCount = lowDiffSeiresCount + 1 if currentSeriesCount > 0 else lowDiffSeiresCount
                    currentSeriesCount = 0

            tmpAllTrain.at[userNo, 'maxSeriesLowDiffCount'] = maxSeriesLowDiffCount
            tmpAllTrain.at[userNo, 'lowDiffSeiresCount'] = lowDiffSeiresCount
        featurized['maxSeriesLowDiffCount'] = tmpAllTrain['maxSeriesLowDiffCount']
        featurized['lowDiffSeiresCount'] = tmpAllTrain['lowDiffSeiresCount']

        featurized = pd.concat([featurized, range_diff], axis=1)  # type: DataFrame
        featurized = featurized.fillna(0)
        return featurized

    @staticmethod
    def consno(train_data):
        """
        :param train_data: DataFrame
        :return:
        """
        consNoMapping = train_data['CONS_NO']
        featurized = DataFrame()
        # 编号trick
        # 每一位
        featurized['consno1'] = consNoMapping // 1000000000 % 10
        featurized['consno2'] = consNoMapping // 100000000 % 10
        featurized['consno3'] = consNoMapping // 10000000 % 10
        featurized['consno4'] = consNoMapping // 1000000 % 10
        featurized['consno5'] = consNoMapping // 100000 % 10
        featurized['consno6'] = consNoMapping // 10000 % 10
        featurized['consno7'] = consNoMapping // 1000 % 10
        featurized['consno8'] = consNoMapping // 100 % 10
        featurized['consno9'] = consNoMapping // 10 % 10
        featurized['consno10'] = consNoMapping // 1 % 10
        # 后几位
        featurized['consnoLast2'] = consNoMapping % 10
        featurized['consnoLast3'] = consNoMapping % 100
        featurized['consnoLast4'] = consNoMapping % 1000
        return featurized