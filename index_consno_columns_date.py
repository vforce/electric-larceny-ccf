#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 11 01:08:58 2016

@author: ash-wang
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
test=pd.read_csv('/home/ash-wang/DF/客户用电异常行为分析/客户用电异常行为分析/test.csv',index_col=0)
all_user=pd.read_csv('/home/ash-wang/DF/客户用电异常行为分析/客户用电异常行为分析/all_user_yongdian_data_2015.csv',index_col=0,header=0)
test_full=all_user.loc[list(test.index)]
test_full['CONS_NO']=test_full.index
test_full['DATA_DATE']=test_full['DATA_DATE'].apply(lambda x:pd.Timestamp(x))
test_full=test_full.sort(['CONS_NO','DATA_DATE'])
test_full=test_full.set_index('DATA_DATE')
test_full=test_full.drop(['KWH_READING','KWH_READING1'],axis=1)
test_full2=pd.DataFrame(columns=test_full.index.unique(),index=test_full['CONS_NO'].unique())

for a,b in test_full.iterrows():
    test_full2.loc[b['CONS_NO'],a]=b['KWH']
