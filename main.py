# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from pandas import DataFrame
from PUAdapter import PUAdapter

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import recall_score
from sklearn.decomposition import PCA

def featurize(all_train, consNoMapping) :
    featurized = DataFrame()
    featurized['std'] = all_train.std(axis=1)
    featurized['nonCount'] = all_train.T.isnull().sum()
    featurized['bigValue'] = all_train[all_train > 150].count(axis=1)

    #较前日比低变化率的次数
    pct_change = all_train.pct_change(axis=1).abs()
    featurized['low_diff_count'] = pct_change[pct_change < 0.2].count(axis=1)
    featurized['<0.2'] = all_train[all_train < 0.2].count(axis=1)
    featurized['<5'] = all_train[all_train < 5].count(axis=1)
    featurized['5'] = all_train[all_train > 5].count(axis=1)
    featurized['10'] = all_train[all_train > 10].count(axis=1)
    featurized['15'] = all_train[all_train > 15].count(axis=1)
    featurized['20'] = all_train[all_train > 20].count(axis=1)
    featurized['25'] = all_train[all_train > 25].count(axis=1)
    featurized['30'] = all_train[all_train > 30].count(axis=1)
    featurized['40'] = all_train[all_train > 40].count(axis=1)
    featurized['50'] = all_train[all_train > 50].count(axis=1)
    featurized['60'] = all_train[all_train > 60].count(axis=1)
    featurized['70'] = all_train[all_train > 70].count(axis=1)
    featurized['80'] = all_train[all_train > 80].count(axis=1)
    featurized['90'] = all_train[all_train > 90].count(axis=1)
    featurized['100'] = all_train[all_train > 100].count(axis=1)
    featurized['100-150'] = featurized['100'] - featurized['bigValue']
    range = pd.concat([ featurized['5'],featurized['10'],featurized['15'],featurized['20'],featurized['25'],featurized['30'],
                        featurized['40'], featurized['50'],featurized['60'], featurized['70'] ,featurized['80'], featurized['90'],
                        featurized['100']
                        ],axis = 1)
    range_diff =range.diff(axis = 1).abs()
    range_diff.rename(columns={'5':'>5','10': '5-10','15': '10-15', '20': '15-20', '25': '20-25', '30': '25-30',
                                                           '40': '30-40', '50': '40-50', '60': '50-60', '70': '60-70', '80': '70-80',
                                                           '90': '80-90', '100': '90-100'
                                                           }, inplace = True)

    #基于样本变化百分比
    pct_change = all_train.pct_change(axis=1).abs()
    pct_change = pct_change.replace('inf',0)
    featurized['pct_std'] = pct_change.std(axis=1)
    featurized['pct<0.5'] = pct_change[pct_change < 0.5].count(axis=1)
    featurized['pct<1'] = pct_change[pct_change < 1].count(axis=1)
    featurized['pct>1.5'] = pct_change[pct_change > 1.5].count(axis=1)
    featurized['pct>3'] = pct_change[pct_change > 3].count(axis=1)
    featurized['pct(0.5,1)]'] = featurized['pct<1'] - featurized['pct<0.5']
    featurized['pct(1,1.5)'] = pct_change[pct_change < 1.5].count(axis=1) - featurized['pct<1']
    featurized['pct(1.5,3)'] = featurized['pct>1.5'] - featurized['pct>3']

    # 基于样本变化百分比的百分比
    pct2_change = pct_change.pct_change(axis=1).abs()
    pct2_change = pct2_change.replace('inf',0)
    featurized['pct2_std < 1'] = pct2_change.std(axis = 1)< 1
    featurized['pct2>50'] = pct2_change[pct2_change >50 ].count(axis=1)

    # 较前日比低变化率的次数
    featurized['low_diff_count'] = pct_change[pct_change < 0.2].count(axis=1)

    #平坡的最大长度、平坡次数
    tmpAllTrain = all_train
    tmpAllTrain['maxSeriesLowDiffCount'] = 0
    tmpAllTrain['lowDiffSeiresCount'] = 0
    for userNo, row in all_train.iterrows() :
        maxSeriesLowDiffCount = 0
        lowDiffSeiresCount = 0

        currentSeriesCount = 0
        for date in row.index[1:] :
            currentValue = row[date]
            if currentValue < 0.2 :
                currentSeriesCount += 1
            else :
                maxSeriesLowDiffCount = currentSeriesCount if currentSeriesCount > maxSeriesLowDiffCount else maxSeriesLowDiffCount
                lowDiffSeiresCount = lowDiffSeiresCount + 1 if currentSeriesCount > 0 else lowDiffSeiresCount
                currentSeriesCount = 0

        tmpAllTrain.at[userNo, 'maxSeriesLowDiffCount'] = maxSeriesLowDiffCount
        tmpAllTrain.at[userNo, 'lowDiffSeiresCount'] = lowDiffSeiresCount
    featurized['maxSeriesLowDiffCount'] = tmpAllTrain['maxSeriesLowDiffCount']
    featurized['lowDiffSeiresCount'] = tmpAllTrain['lowDiffSeiresCount']

    featurized = pd.concat([featurized,range_diff],axis = 1)
    featurized = featurized.fillna(0)
    return featurized



# Main
larceny_data = pd.read_csv("./input/train_data_true.csv")
unknown_data = pd.read_csv("./input/train_data_false.csv")
testData = pd.read_csv("./input/test_data.csv")
# 将数据73开
shuffled_larceny = pd.Series(larceny_data.index).as_matrix()
shuffled_unknwon = pd.Series(unknown_data.index).as_matrix()
np.random.shuffle(shuffled_larceny)
np.random.shuffle(shuffled_unknwon)
train_larceny = larceny_data.loc[shuffled_larceny[0: np.ceil(len(larceny_data) * 0.7).astype(int)], :]
cross_validation_larceny = larceny_data.loc[shuffled_larceny[np.ceil(len(larceny_data) * 0.7).astype(int) + 1:], :]
train_unknwon = unknown_data.loc[shuffled_unknwon[0:np.ceil(len(unknown_data) * 0.7).astype(int)], :]
cross_validation_unknown = unknown_data.loc[shuffled_unknwon[np.ceil(len(unknown_data) * 0.7).astype(int) + 1:], :]
# 构建训练集
train_data = pd.concat([train_larceny.T[1:].T, train_unknwon.T[1:].T])
train_larceny['class'] = 1
train_unknwon['class'] = -1
consNoMapping = pd.concat([train_larceny.loc[:, 'CONS_NO'], train_unknwon.loc[:, 'CONS_NO']], ignore_index=True)
# 构建训练集标签
train_label = pd.concat([train_larceny['class'], train_unknwon['class']])
# 提取训练集特征
featurized_train = featurize(all_train=train_data, consNoMapping=consNoMapping)

# 构建交叉验证测试集
cross_validation_data = pd.concat([cross_validation_larceny.T[0:].T, cross_validation_unknown.T[0:].T])
cross_validation_larceny['class'] = 1
cross_validation_unknown['class'] = -1
# 构建交叉验证集标签
cross_validation_label = pd.concat([cross_validation_larceny['class'], cross_validation_unknown['class']], ignore_index=True)
# 提取交叉验证集特征
featurized_cross = featurize(all_train=cross_validation_data, consNoMapping=pd.concat([cross_validation_larceny['CONS_NO'], cross_validation_unknown['CONS_NO']], ignore_index=True))

# 所有训练集特征（训练集特征+交叉验证集特征）
featurized_Alltrain = pd.concat([featurized_train, featurized_cross])
featurized_Alltrain_label = pd.concat([train_label, cross_validation_label])

# 分类器
randomForest = PUAdapter(RandomForestClassifier(), hold_out_ratio=0.2)
gradientBoostingTree = PUAdapter(GradientBoostingClassifier(), hold_out_ratio=0.2)
classifiers = [randomForest, gradientBoostingTree]
pca = PCA(n_components=6)
pca.fit(featurized_train)

# 训练
for classifier in classifiers :
    classifier.fit(featurized_Alltrain.as_matrix(), featurized_Alltrain_label.as_matrix())

train_result = []
for classifier in classifiers :
    train_result.append(classifier.predict_proba(featurized_Alltrain.as_matrix()))
train_result = DataFrame(train_result).T
# 构建集成学习
final_classifier = PUAdapter(RandomForestClassifier())
final_classifier.fit(train_result.as_matrix(), featurized_Alltrain_label.as_matrix())

# 测试集特征
featurized_test = featurize(all_train=testData.T[0:].T, consNoMapping=testData.loc[:, 'CONS_NO'])

test_result = []
for classifier in classifiers :
    test_result.append(classifier.predict_proba(featurized_test.as_matrix()))
test_result = DataFrame(test_result).T
predict = final_classifier.predict_proba(test_result)

result = pd.DataFrame({
    "CONS_NO": testData["CONS_NO"],
    "PROBA": predict.T
})

csv = result.sort_values(by='PROBA', ascending=False)['CONS_NO']
csv.to_csv('result.csv', index=False)