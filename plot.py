# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt
import shutil
import os

# 画图
analyze = pd.read_csv('result/incorrect_predict.csv')
larceny_data = pd.read_csv("input/train_data_true.csv")
larceny_data.fillna(-1)
shutil.rmtree('incorrect_fig')
os.mkdir('incorrect_fig')
for cons_no in analyze.CONS_NO :
    Y = larceny_data[larceny_data.CONS_NO == cons_no].T[1:]
    X = range(0, 343, 1)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(X, Y.as_matrix(), 'o-')
    plt.savefig("incorrect_fig/" + str(cons_no) + ".png")