import pandas as pd
import numpy as np
from pandas import DataFrame
from PUAdapter import PUAdapter
from sklearn.model_selection import train_test_split
class inputData :
    @staticmethod
    def cross_validation():
        larceny_data = pd.read_csv("./input/train_data_true_wyc.csv")
        unknown_data = pd.read_csv("./input/train_data_false_wyc.csv")
        shuffled_larceny = pd.Series(larceny_data.index).as_matrix()
        shuffled_unknwon = pd.Series(unknown_data.index).as_matrix()
        np.random.shuffle(shuffled_larceny)
        np.random.shuffle(shuffled_unknwon)
        train_larceny = larceny_data.loc[shuffled_larceny[0: np.ceil(len(larceny_data) * 0.7).astype(int)], :]
        cross_validation_larceny = larceny_data.loc[shuffled_larceny[np.ceil(len(larceny_data) * 0.7).astype(int) + 1:],
                                   :]
        train_unknwon = unknown_data.loc[shuffled_unknwon[0:np.ceil(len(unknown_data) * 0.7).astype(int)], :]
        cross_validation_unknown = unknown_data.loc[shuffled_unknwon[np.ceil(len(unknown_data) * 0.7).astype(int) + 1:],
                                   :]
        train_data = pd.concat([train_larceny, train_unknwon], ignore_index=True)
        train_larceny['class'] = 1
        train_unknwon['class'] = -1
        train_label = pd.concat([train_larceny['class'], train_unknwon['class']], ignore_index=True)
        cross_validation_data = pd.concat([cross_validation_larceny, cross_validation_unknown], ignore_index=True)
        cross_validation_larceny['class'] = 1
        cross_validation_unknown['class'] = -1
        cross_validation_label = pd.concat([cross_validation_larceny['class'], cross_validation_unknown['class']],
                                           ignore_index=True)
        return (train_data, cross_validation_data, train_label, cross_validation_label)

    @staticmethod
    def product():
        larceny_data = pd.read_csv("./input/train_data_true_wyc.csv")
        unknown_data = pd.read_csv("./input/train_data_false_wyc.csv")
        train_data = pd.concat([larceny_data, unknown_data], ignore_index=True)  # type: DataFrame
        larceny_data['class'] = 1
        unknown_data['class'] = -1
        train_label = pd.concat([larceny_data['class'], unknown_data['class']], ignore_index=True)  # type:DataFrame
        test_data = pd.read_csv("./input/test_data_wyc.csv")
        test_label = None
        return (DataFrame(train_data), DataFrame(test_data), DataFrame(train_label), DataFrame(test_label))