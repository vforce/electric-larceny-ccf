# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import multiprocessing
from featurerize import *
from inputData import *
from pandas import DataFrame
from PUAdapter import PUAdapter

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
import matplotlib.pyplot as plt
from sklearn.metrics import recall_score
from sklearn.decomposition import PCA

# Main
# 读入数据
ON_PROD = input("Is on production envrionment? (0, 1) :")
if ON_PROD == 1 :
    train_data, test_data, train_label, test_label = inputData.product()
else :
    train_data, test_data, train_label, test_label = inputData.cross_validation()
if __name__ == '__main__':
    multiprocessing.freeze_support()
    pool = multiprocessing.Pool(processes=8)
    # 提取特征
    processes = []
    processes.append(pool.apply_async(func=featurize_common, args=[train_data]))
    processes.append(pool.apply_async(func=featurize_consno, args=[train_data]))
    processes.append(pool.apply_async(func=featurize_common, args=[test_data]))
    processes.append(pool.apply_async(func=featurize_consno, args=[test_data]))
    pool.close()
    pool.join()
    feature_train_common = processes[0].get()
    feature_train_consno = processes[1].get()
    feature_test_common = processes[2].get()
    feature_test_consno = processes[3].get()
    print 'Featurize Done.'
    # 分类器
    rf = RandomForestClassifier(n_jobs=4, random_state=100, n_estimators=5000, verbose=True)
    classifier_common1 = PUAdapter(rf)
    classifier_consno = PUAdapter(RandomForestClassifier(n_jobs=4, random_state=100, n_estimators=5000, verbose=True))
    # 训练
    classifier_common1.fit(feature_train_common.as_matrix(), train_label.as_matrix())
    classifier_consno.fit(feature_train_consno.as_matrix(), train_label.as_matrix())
    feature_csv = pd.concat([DataFrame(feature_train_common.columns.values.tolist()), DataFrame(rf.feature_importances_)], axis=1) # type: DataFrame
    feature_csv.to_csv('result/feature_importance.csv', index=False)
    print 'Feature Importance Exported.'
    # 预测
    predict_common1 = classifier_common1.predict_proba(feature_test_common.as_matrix())
    predict_consno = classifier_consno.predict_proba(feature_test_consno.as_matrix())
    # 集成
    predict = 0.2 * predict_common1 + 0.8 * predict_consno # type: DataFrame
    if (ON_PROD == 1) :
        result = pd.DataFrame({
            "CONS_NO": test_data["CONS_NO"],
            "PROBA": predict.T
        })
        # 生成csv
        result.sort_values(by='PROBA', ascending=False)['CONS_NO'].to_csv('result/result_v2.csv', index=False)
        print 'CSV Result Exported.'
    else :
        # 分析错误预测
        result = pd.DataFrame({
            "CONS_NO": test_data["CONS_NO"],
            "PROBA": predict.T,
            "LABEL": test_label
        })
        analyze = result[result.LABEL == 1]
        analyze = analyze[analyze.PROBA < 0.5]
        analyze.to_csv('result/incorrect_predict.csv', index=None)
        print 'Incorrect Predict Result Exported.'